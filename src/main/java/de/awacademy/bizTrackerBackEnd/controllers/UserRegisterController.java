package de.awacademy.bizTrackerBackEnd.controllers;


import de.awacademy.bizTrackerBackEnd.dtos.UserDto;
import de.awacademy.bizTrackerBackEnd.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UserRegisterController {

    private UserService userService;

    @Autowired
    public UserRegisterController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/newUser")
    public long postUser(@RequestBody UserDto userDto){
        return this.userService.postUser(userDto);
    }

}
