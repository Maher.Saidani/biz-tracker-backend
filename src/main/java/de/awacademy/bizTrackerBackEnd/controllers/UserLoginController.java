package de.awacademy.bizTrackerBackEnd.controllers;

import de.awacademy.bizTrackerBackEnd.dtos.UserDto;
import de.awacademy.bizTrackerBackEnd.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UserLoginController {

    private UserService userService;

    @Autowired
    public UserLoginController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/loginUser")
    public UserDto loginUser(@RequestBody UserDto userDto){
        UserDto userDto1 = this.userService.loginUser(userDto);
        System.out.println("Controller DTO: " +userDto1.getId());

        return userDto1;
    }

    @GetMapping("/home/{id}")
    public UserDto getUser(@PathVariable("id") String id)
    {
        boolean isNumeric = (id != null && id.matches("[0-9]+"));
        return isNumeric ? this.userService.getUser(Long.parseLong(id)): null;
    }

}
