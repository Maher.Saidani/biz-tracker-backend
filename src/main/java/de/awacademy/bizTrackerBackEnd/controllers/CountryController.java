package de.awacademy.bizTrackerBackEnd.controllers;


import de.awacademy.bizTrackerBackEnd.dtos.charts.CountryDto;
import de.awacademy.bizTrackerBackEnd.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class CountryController {

    private CountryService countryService;

    @Autowired
    CountryController (CountryService countryService) {
        this.countryService = countryService;
    }

    @GetMapping("/getCountries")
    public List<CountryDto> getCountries () {
        return countryService.getFakeCountries(5);
    }
}
