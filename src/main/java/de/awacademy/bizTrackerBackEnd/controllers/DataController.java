package de.awacademy.bizTrackerBackEnd.controllers;

import de.awacademy.bizTrackerBackEnd.dtos.CustomerDto;
import de.awacademy.bizTrackerBackEnd.dtos.OpportunityDto;
import de.awacademy.bizTrackerBackEnd.services.CustomerService;
import de.awacademy.bizTrackerBackEnd.services.OpportunityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class DataController {
    private CustomerService customerservice;
    private OpportunityService opportunityService;

    @Autowired
    DataController(CustomerService service, OpportunityService oService) {
        this.customerservice = service;
        this.opportunityService = oService;
    }

    @GetMapping("/getCustomers")
    public ArrayList<CustomerDto> getAllCustomers () {
        return customerservice.getAllCustomers();
    }

    @GetMapping("/getOpportunities")
    public ArrayList<OpportunityDto> getAllOpportunities () {
        return opportunityService.getAllOpportunities();
    }

}
