package de.awacademy.bizTrackerBackEnd.repositories;

import de.awacademy.bizTrackerBackEnd.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "select u from User u where u.email = :email")
    User checkRegisterEmail(@Param("email") String email);

}
