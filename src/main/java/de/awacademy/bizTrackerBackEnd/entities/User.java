package de.awacademy.bizTrackerBackEnd.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.Date;


@Entity
@Data //Von Lombock: generiert getter und setter und to-String-Methode
public class User {

    @Id
    @GeneratedValue
    private long id;

    private String name;
    private String email;
    private Date birthDate;
    private String password;
    private String type;
    private String registredDate;

    private String externalUsername;
    private String externalPassword;
    private String externalSecurityToken;

    public User() {
    }

    public User(String name) {
        this.name = name;
    }

    public User(Long id, String name, String type, String email, String password,
                      String externalUsername, String externalPassword, String externalSecurityToken) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.email = email;
        this.password = password;
        this.externalUsername = externalUsername;
        this.externalPassword = externalPassword;
        this.externalSecurityToken = externalSecurityToken;
    }
}
