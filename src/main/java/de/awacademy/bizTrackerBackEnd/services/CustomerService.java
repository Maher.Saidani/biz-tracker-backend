package de.awacademy.bizTrackerBackEnd.services;

import de.awacademy.bizTrackerBackEnd.dtos.CustomerDto;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;


@Service
public class CustomerService {
    private APIConnectionService connectionService;
    private String localURL = "http://localhost:8081/customer";
    private String deployedAPI = "http://biztracker-awproject.us-e2.cloudhub.io/customer";

    @Autowired
    public CustomerService(APIConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    public CustomerDto createCustomerFromJson (JSONObject object){
        return new CustomerDto(
                object.get("Id").toString(),
                object.get("Name").toString(),
                object.get("Phone").toString(),
                object.get("OwnerId").toString(),
                object.getJSONObject("BillingAddress").get("street").toString(),
                object.getJSONObject("BillingAddress").get("country").toString(),
                object.getJSONObject("BillingAddress").get("postalCode").toString(),
                object.getJSONObject("BillingAddress").get("city").toString(),
                object.getJSONObject("BillingAddress").get("country").toString(),
                object.getJSONObject("BillingAddress").getDouble("longitude"),
                object.getJSONObject("BillingAddress").getDouble("latitude"),
                object.get("AccountNumber").toString()
        );
    }

    public ArrayList<CustomerDto> getAllCustomers () {
        ArrayList<CustomerDto> customers = new ArrayList<>();
        String payload = "";
        try {
            payload = connectionService.getJsonFromUrl(deployedAPI);
            JSONArray arr =  new JSONObject(payload).getJSONArray("content");
            for (int i = 0; i < arr.length(); i++) {
                customers.add(createCustomerFromJson(arr.getJSONObject(i)));
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return customers;
    }

}
