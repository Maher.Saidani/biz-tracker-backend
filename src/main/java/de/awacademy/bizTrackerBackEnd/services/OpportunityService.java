package de.awacademy.bizTrackerBackEnd.services;

import de.awacademy.bizTrackerBackEnd.dtos.OpportunityDto;
import de.awacademy.bizTrackerBackEnd.dtos.OpportunityLineDto;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class OpportunityService {
    private APIConnectionService connectionService;
    private String localOpportunityURL = "http://localhost:8081/opportunity";
    private String localProductURL = "http://localhost:8081/line";
    private String deployedOpportunityURL = "http://biztracker-awproject.us-e2.cloudhub.io/opportunity";
    private String deployedProductURL = "http://biztracker-awproject.us-e2.cloudhub.io/line";

    @Autowired
    OpportunityService (APIConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    public ArrayList<OpportunityDto> getAllOpportunities () {
        ArrayList<OpportunityDto> opportunities = new ArrayList<>();
        String payload = "";
        try {
            payload = connectionService.getJsonFromUrl(deployedOpportunityURL);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        JSONArray arr =  new JSONArray(payload);
        for (int i = 0; i < arr.length(); i++) {
            opportunities.add(createOpportunityFromJson(arr.getJSONObject(i)));
        }
        addProductToOpportunities(opportunities);
        return opportunities;
    }

    private void addProductToOpportunities (ArrayList<OpportunityDto> opportunities) {
        String payload = "";
        try {
            payload = connectionService.getJsonFromUrl(deployedProductURL);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        JSONArray arr =  new JSONArray(payload);
        for (int i = 0; i < arr.length(); i++) {
            OpportunityLineDto product = createProductFromJson(arr.getJSONObject(i));
            for (int j = 0; j < opportunities.size(); j++){
                if (product.getOpportunityId().equals(opportunities.get(j).getId())) {
                    opportunities.get(j).addProduct(product);
                }
            }
        }
    }

    private OpportunityDto createOpportunityFromJson (JSONObject object){
        return new OpportunityDto(
                object.get("Id").toString(),
                object.get("AccountId").toString(),
                object.getDouble("Amount"),
                object.getString("StageName"),
                object.getBoolean("IsWon"),
                object.get("CreatedDate").toString(),
                object.get("CloseDate").toString()
        );
    }

    private OpportunityLineDto createProductFromJson (JSONObject object){
        return new OpportunityLineDto(
                object.get("Id").toString(),
                object.get("OpportunityId").toString(),
                object.get("Name").toString(),
                object.get("ProductCode").toString(),
                object.get("Description").toString(),
                Double.parseDouble(object.get("UnitPrice").toString()),
                Double.parseDouble(object.get("TotalPrice").toString()),
                Double.parseDouble(object.get("Quantity").toString())
        );
    }
}
