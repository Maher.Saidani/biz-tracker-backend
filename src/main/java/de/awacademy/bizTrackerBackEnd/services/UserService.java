package de.awacademy.bizTrackerBackEnd.services;

import de.awacademy.bizTrackerBackEnd.dtos.UserDto;
import de.awacademy.bizTrackerBackEnd.entities.User;
import de.awacademy.bizTrackerBackEnd.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.*;

import java.time.LocalDateTime;


@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }



    private boolean checkRegisterEmail(String email){
        return this.userRepository.checkRegisterEmail(email) != null;
    }

    public long postUser(UserDto userDto) {

        User user = mapToEntity(userDto);

        if (checkRegisterEmail(userDto.getEmail()))
            return -1;
        else
            this.userRepository.save(user);

        return user.getId();
    }

    private UserDto mapToDto(User user){
        UserDto userDto = new UserDto();

        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setEmail(user.getEmail());
        userDto.setRegistredDate(LocalDateTime.parse(user.getRegistredDate()));
        userDto.setBirthDate(user.getBirthDate());
        userDto.setType(user.getType());
        userDto.setPassword(user.getPassword());

        return userDto;
    }

    private User mapToEntity(UserDto userDto) {

        User user = new User();

        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());
        user.setRegistredDate(userDto.getRegistredDate().toString());
        user.setBirthDate(userDto.getBirthDate());
        user.setType(userDto.getType());

        user.setPassword(BCrypt.hashpw(userDto.getPassword(), BCrypt.gensalt()));

        return user;
    }

    public UserDto loginUser(UserDto userDto) {
        User user = userRepository.checkRegisterEmail(userDto.getEmail());

        UserDto loggedUserDto;
        if (user !=null && BCrypt.checkpw(userDto.getPassword(), user.getPassword())){
            userRepository.save(user);
            loggedUserDto = mapToDto(user);
        }
        else
            loggedUserDto = null;

        System.out.println("Service DTO: " +userDto.getId());
        System.out.println("Service Entity: " +user.getId());

        return loggedUserDto;
    }

    public UserDto getUser(long id) {
        UserDto userDto = userRepository.existsById(id) ? mapToDto(userRepository.findById(id).get()) : null;

        return userDto;
    }

}
