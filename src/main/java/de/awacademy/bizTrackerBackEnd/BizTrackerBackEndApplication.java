package de.awacademy.bizTrackerBackEnd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication //dafür zuständig ein spring-boot-container zu starten
public class BizTrackerBackEndApplication {

	/***
	 * main Method to bootstrap the Spring application
	 * */
	public static void main(String[] args) {
		SpringApplication.run(BizTrackerBackEndApplication.class, args);
	}
}
