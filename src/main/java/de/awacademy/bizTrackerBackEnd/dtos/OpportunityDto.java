package de.awacademy.bizTrackerBackEnd.dtos;

import lombok.Data;

import java.util.ArrayList;

@Data //Von Lombock: generiert getter und setter und to-String-Methode
public class OpportunityDto {
    private String id;
    private String accountId;
    private double amount;
    private String stage;
    private boolean isWon;
    private String createdDate;
    private String closeDate;
    private ArrayList<OpportunityLineDto> products;

    public OpportunityDto(String id, String accountId, double amount, String stage, boolean isWon,
                          String createdDate, String closeDate) {
        this.id = id;
        this.accountId = accountId;
        this.amount = amount;
        this.stage = stage;
        this.isWon = isWon;
        this.createdDate = createdDate.substring(0,10);
        this.closeDate = closeDate.substring(0,10);
        this.products = new ArrayList<>();
    }

    public boolean addProduct (OpportunityLineDto product) {
        return this.products.add(product);
    }
}
