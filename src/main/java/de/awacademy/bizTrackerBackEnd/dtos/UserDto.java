package de.awacademy.bizTrackerBackEnd.dtos;


import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

@Data //Von Lombock: generiert getter und setter und to-String-Methode
public class UserDto {
    
    private long id;
    private String name;
    private String email;
    private Date birthDate;
    private String password;
    private String type;
    private boolean online;
    private LocalDateTime registredDate;
    private String externalUsername;
    private String externalPassword;
    private String externalSecurityToken;


    public UserDto() {
        this.registredDate = LocalDateTime.now();
    }

    public UserDto(Long id, String name, String type, String email, String password,
                   String externalUsername, String externalPassword, String externalSecurityToken) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.email = email;
        this.password = password;
        this.externalUsername = externalUsername;
        this.externalPassword = externalPassword;
        this.externalSecurityToken = externalSecurityToken;
    }
}
