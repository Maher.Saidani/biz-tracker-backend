package de.awacademy.bizTrackerBackEnd.dtos;

import lombok.Data;

import java.util.ArrayList;

@Data
public class CustomerDto {
    private String id;
    private String accountNumber;
    private String name;
    private String addressLine1;
    private String addressLine2;
    private String addressCity;
    private String addressZipCode;
    private String addressCountry;
    private String phone;
    private String accountOwner;
    private ArrayList<Long> orders;
    private double longitude;
    private double latitude;

    CustomerDto () {}

    public CustomerDto(String id, String name, String addressLine1, String addressLine2, String addressCity,
                       String addressZipCode, String addressCountry, String phone, String accountOwner,
                       ArrayList<Long> orders) {
        this.id = id;
        this.name = name;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.addressCity = addressCity;
        this.addressZipCode = addressZipCode;
        this.addressCountry = addressCountry;
        this.phone = phone;
        this.accountOwner = accountOwner;
        this.orders = orders;
        this.latitude = (Math.random()*8 + 47);
        this.longitude = (Math.random()*8 + 6);
    }

    public CustomerDto(String id, String name, String phone, String accountOwner, String addressLine1, String addressLine2,
                          String addressZipCode, String addressCity, String addressCountry,
                          double longitude, double latitude, String accountNumber) {
        this.id = id;
        this.name = name;
        this.accountOwner = accountOwner;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.addressZipCode = addressZipCode;
        this.addressCity = addressCity;
        this.addressCountry = addressCountry;
        this.phone = phone;
        this.latitude = latitude;
        this.longitude = longitude;
        this.accountNumber = accountNumber;
    }
}
