package de.awacademy.bizTrackerBackEnd.dtos.charts;

import java.util.Arrays;
import java.util.Random;

public class StatDto {

    private String name;
    private int value;

    public StatDto() {
    }

    public static StatDto[] getFakeStats() {

        StatDto[] stats = new StatDto[2];
        Random random = new Random();

        for (int i=0; i<2; i++){
            StatDto stat = new StatDto();
            stat.name = i%2==0?"2017":"2018";
            stat.value = 1 + (i%2==0? random.nextInt(3_000_000)
                    : random.nextInt(3_000_000- stats[0].value));
            stats[i] = stat;
        }

        return stats;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
