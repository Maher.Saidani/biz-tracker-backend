package de.awacademy.bizTrackerBackEnd.dtos.charts;

import lombok.Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Data
public class CountryDto {

    private String name;
    private StatDto[] series;


    public CountryDto() {
    }

    public static List<CountryDto> getFakeCountries(int size){
        List<CountryDto> countries = new ArrayList<>();
        List<String> names = new ArrayList<>(List.of("USA","CANADA","BRASIL","MEXICO","FRANCE","GERMANY",
                "HOLLAND","ENGLAND","MAROCCO","ALGERIA","TUNISIA","EGYPT","CHINA","INDIA",
                "JAPAN", "SOUTH KOREA", "NEW ZEALAND", "AUSTRIA"));

        Collections.shuffle(names);
        for (int i=0; i<size; i++){
            CountryDto country = new CountryDto();
            country.name = names.get(i);
            country.series = StatDto.getFakeStats();
            countries.add(country);
        }

        return countries;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StatDto[] getSeries() {
        return series;
    }

    public void setSeries(StatDto[] series) {
        this.series = series;
    }
}
