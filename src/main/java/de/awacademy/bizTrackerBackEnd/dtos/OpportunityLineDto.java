package de.awacademy.bizTrackerBackEnd.dtos;

import lombok.Data;

@Data
public class OpportunityLineDto {
    private String id;
    private String opportunityId;
    private String name;
    private String productCode;
    private String description;
    private double unitPrice;
    private double totalPrice;
    private double quantity;

    public OpportunityLineDto(String id, String opportunityId, String name, String productCode, String description,
                              double unitPrice, double totalPrice, double quantity) {
        this.id = id;
        this.opportunityId = opportunityId;
        this.name = name;
        this.productCode = productCode;
        this.description = description;
        this.unitPrice = unitPrice;
        this.totalPrice = totalPrice;
        this.quantity = quantity;
    }
}
